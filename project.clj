(defproject index-sync "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [hitchhiker-tree "0.1.0-SNAPSHOT"]
                 [io.replikativ/konserve "0.4.11"]
                 [io.replikativ/superv.async "0.2.9"]])
