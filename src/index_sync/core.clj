(ns index-sync.core
  (:require [hitchhiker.konserve :as kons]
            [hitchhiker.tree.messaging :as msg]
            [hitchhiker.tree.core :as core]
            [konserve.memory :refer [new-mem-store]]
            [konserve.filestore :refer [new-fs-store delete-store]]
            [konserve.core :as k]
            [clojure.data :refer [diff]]
            [superv.async :refer [S <?? go-try <? <<??]]
            [clojure.core.async :refer [<!!] :as async])
  (:import [konserve.filestore FileSystemStore]))

(def store (kons/add-hitchhiker-tree-handlers (<!! (new-fs-store "/tmp/index-sync"))))

(comment
  (delete-store "/tmp/index-sync"))

(def backend (kons/->KonserveBackend store))

(def init-tree
  (<!! (core/reduce<
        (fn [t i] (msg/insert t i i))
        (<!! (core/b-tree (core/->Config 300 100 200)))
        (range 1 4000))))

(def A-flushed (:tree (<!! (core/flush-tree init-tree backend))))

;; TODO
;; write through cache
;; 
;; read after flush
;; read through cache

(def root-key (kons/get-root-key A-flushed))

(async/poll! (:storage-addr A-flushed))

(def stored-tree (<!! (kons/create-tree-from-root-key store root-key)))

(msg/lookup-fwd-iter stored-tree 3)

(def A stored-tree)

(msg/lookup-fwd-iter A 0)
(msg/lookup-fwd-iter B 0)

(<!! (core/reduce< (fn [t i] (msg/insert t i i)) A (range 20 22)))

(def B-mem (<!! (core/reduce< (fn [t i] (msg/insert t i i)) stored-tree (range 4000 5000))))

(def B-flushed
  (time
   (<?? S
        (core/flush-tree
         B-mem
         backend))))

(def B
  (<?? S (kons/create-tree-from-root-key
          store
          (kons/get-root-key (:tree B-flushed)))))

(:op-buf (->> B :children second core/resolve (<?? S)))

(->> B :children second core/resolve (<?? S) :op-buf)

(->> B :op-buf)

(->> A :op-buf)



;; 1. determine delta, KC


;; 2. get only-in-b children


(defn missing-in-A [A B]
  (->> (clojure.data/diff A B)
       second ;; B
       :children
       (map :konserve-key)
       (filter identity)))


(missing-in-A A B)

;; get all index-node konserve keys in A



(defn extract-node-keys
  "Extract all tree node keys of A."
  [A]
  (when (core/index-node? A)
    (let [ks (map :konserve-key (:children A))]
      (concat ks (->> ks
                      (map #(k/get-in store [%]))
                      async/merge
                      (<<?? S)
                      (mapcat #(extract-node-keys %)))))))



(defn extract-ops [in-a ops ks] ;; ks from B
  (let [vs (->> ks
                ;; TODO fetch remote
                (map #(k/get-in store [%]))
                async/merge
                (<<?? S))
        new-ops (->> (mapcat :op-buf vs)
                     (map (fn [op] [(:key op) (:value op)])))
        cs (filter (comp not in-a)
                   (mapcat :children vs))]
    (if (some core/index-node? vs)
      (recur in-a
             (concat ops new-ops)
             (map :konserve-key cs))
      (concat ops cs))))


(comment
  (count (extract-ops
          (set (extract-node-keys A))
          (map (fn [op] [(:key op) (:value op)]) (:op-buf B))
          (missing-in-A A B)))

  (->>
   (extract-ops
    (set (extract-node-keys A))
    (map (fn [op] [(:key op) (:value op)]) (:op-buf B))
    (missing-in-A A B))
   (map first)
   distinct
   sort
   (drop 299))

  (->> (missing-in-A A B)
       (map #(k/get-in store [%]))
       async/merge
       (<<?? S)
       (map :op-buf)))

;; 3. fetch values K
;; TODO remote interface w. core.async

;; 4.1. fetch binary data-nodes

;; 5. extract ops from buffers on new-nodes C

;; 6. insert ops into A

;; 7. write new A root (commit).



;; 1. create in-memory delta




